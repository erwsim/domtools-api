#!/usr/bin/env python
#coding : utf8

import logging

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

from redis import Redis

app = Flask(__name__)
app.config.from_object('config')
app.url_map.strict_slashes = False

db = SQLAlchemy(app)
r = Redis('localhost')

log = logging.getLogger('DOMTools API')
log.setLevel(app.config['DEBUG_LEVEL'])

ch = logging.StreamHandler()
ch.setLevel(app.config['DEBUG_LEVEL'])

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)

log.addHandler(ch)

from app.controllers.dnshisto import mod_dnshisto as dnshisto_module
from app.controllers.whois import mod_whois as whois_module
from app.controllers.dig import mod_dig as dig_module

app.register_blueprint(dnshisto_module)
app.register_blueprint(whois_module)
app.register_blueprint(dig_module)

db.create_all()
