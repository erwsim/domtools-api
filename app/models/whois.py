#!/usr/bin/env python
# coding: utf-8

from subprocess import run, PIPE

from app import r
from app.models.input import Input
from app import log

class WHOIS():
    def __init__(self, domain):
        self.ttl = 86400 # 1 DAY TTL for cache
        self.domain = Input.clear_domain(domain) # Clear domain name, remove whitespace, if e-mail is provided, only let domain name
        self.tld = self.domain.split('.')[-1]
        self.whois = ''
        self.expire = self.get_expire()
        self.thin_whois = ['com', 'net'] # Thin whois have to be provided here (Verisign TLDs for example)
        self.error = False
        log.debug('Initialize WHOIS for domain %s', self.domain)

    
    def run(self):
        log.info('Display WHOIS for domain %s', self.domain)
        # Check if data is in cache and get it in this case
        if self.is_cached():
            self.whois = self.get_cache().decode('utf-8')
        else:
            # Get whois value, thick must be done first and if domain is in thin value then do thin and replace whois value
            self.thick = self.whois_thick().decode('utf-8')
            self.whois = self.thick
            if self.tld in self.thin_whois and 'No match for' not in self.thick:
                log.debug('Thin domain detected')
                self.whois = self.whois_thin()
                if self.error == False:
                    self.whois = self.whois.decode('utf-8')
                else:
                    self.whois = self.thick

            if not self.error:
                self.set_cache(self.whois)

        simple = self.simple()

    
    def is_cached(self):
        if r.exists(self.domain):
            log.debug('Domain WHOIS is stored in cache')
            return True
        else:
            log.debug('Domain WHOIS is not stored in cache')
            return False

    
    def get_cache(self):
        log.debug('Display cache')
        return r.get(self.domain)

    
    def get_expire(self):
        if self.is_cached():
            ttl = r.ttl(self.domain)
            log.debug('TTL value is %s', ttl)
            return ttl
        else:
            log.debug('Default TTL')
            return self.ttl

    
    def set_cache(self, whois):
        if self.is_cached():
            log.debug('Domain already in cache, will not be cached')
            return False
        else:
            log.debug('Store domain in cache')
            r.set(self.domain, whois)
            r.expire(self.domain, self.ttl)
            return True

    
    def whois_thick(self):
        log.debug('Run THICK WHOIS command')
        self.whois = run(['whois', self.domain], stdout=PIPE)
        return self.whois.stdout.decode('latin-1').encode('utf-8')

    
    def whois_thin(self):
        log.debug('Run THIN WHOIS command')
        lines = self.whois.splitlines()
        for line in lines:
            if 'Registrar WHOIS Server:' in line:
                registrar_whois = line.split(': ')[-1]

                self.whois = run(['whois', '-h', registrar_whois, self.domain], stdout=PIPE)
                whois = self.whois.stdout.decode('latin-1').encode('utf-8')

                if 'Too many connection attempts' in whois.decode('utf-8') or 'Sorry, delay too short since your last request' in whois.decode('utf-8') or whois.decode('utf-8') == '':
                    log.warning('Too many connection attempts or empty whois')
                    self.error = True
                elif 'Connection refused' in whois.decode('utf-8'):
                     log.warning('Blacklisted by %s', registrar_whois)
                     self.error = True
                else:
                    pass
                     
        if 'whois' in locals():
            return whois
        else:
            self.error = True


    def simple(self):
        simple = self.whois.splitlines()

        whois_format = {
                'fr': {
                    'domain': 'domain',
                    'status': 'status',
                    'registrar': 'registrar',
                    'expire': 'Expiry Date',
                    'created': 'created',
                    'last-update': 'last-update',
                    'dns': 'nsserver',
                    'registrant': {
                        'name': 'contact',
                        'organization': 'type',
                        'address': 'address',
                        'country': 'country',
                        'phone': 'phone',
                        'email': 'e-mail'
                        },
                    'admin': {
                        'name': 'contact',
                        'address': 'address',
                        'country': 'country',
                        'phone': 'phone',
                        'email': 'e-mail'
                        },
                    'tech': {
                        'name': 'contact',
                        'address': 'address',
                        'country': 'country',
                        'phone': 'phone',
                        'email': 'e-mail'
                        }
                    },
                'others': {
                    'domain': 'Domain Name',
                    'status': 'Domain Status',
                    'registrar': 'Registrar',
                    'expire': 'Registrar Registration Expiration Date',
                    'created': 'Creation Date',
                    'last-update': 'Updated Date',
                    'dns': 'Nameserver',
                    'registrant': {
                        'name': 'Registrant Name',
                        'organization': 'Registrant Organization',
                        'address': ['Registrant Street', 'Registrant City', 'Registrant State/Province', 'Registrant Postal Code'],
                        'country': 'Registrant Country',
                        'phone': 'Registrant Phone',
                        'email': 'Registrant Email'
                        },
                    'admin': {
                        'name': 'Admin Name',
                        'organization': 'Admin Organization',
                        'address': ['Admin Street', 'Admin City', 'Admin State/Province', 'Admin Postal Code'],
                        'country': 'Admin Country',
                        'phone': 'Admin Phone',
                        'email': 'Admin Email'
                        },
                    'tech': {
                        'name': 'Tech Name',
                        'organization': 'Tech Organization',
                        'address': ['Tech Street', 'Tech City', 'Tech State/Province', 'Tech Postal Code'],
                        'country': 'Tech Country',
                        'phone': 'Tech Phone',
                        'email': 'Tech Email'
                        }
                    }
            }

        if self.tld not in whois_format:
            self.tld = 'others'

        for key in whois_format[self.tld]:
            if key in simple:
                pass
