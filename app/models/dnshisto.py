#!/usr/bin/env python
# coding: utf8

from app import db

class Domain(db.Model):
    __tablename__ = 'domains'

    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(128), unique = True)

    zone = db.relationship('Zone', backref='Zone.id', primaryjoin='Domain.id==Zone.domain_id', lazy='joined')

class Zone(db.Model):
    __tablename__ = 'zones'

    id = db.Column(db.Integer, primary_key = True)
    domain_id = db.Column(db.Integer, db.ForeignKey(Domain.id, onupdate="cascade", ondelete="cascade"), index = True)
    date = db.Column(db.DateTime, default=db.func.current_timestamp())

    record = db.relationship('Record', backref='Record.id', primaryjoin='Zone.id==Record.zone_id', lazy='joined')


class Record(db.Model):
    __tablename__ = 'records'

    id = db.Column(db.Integer, primary_key = True)
    zone_id = db.Column(db.Integer, db.ForeignKey(Zone.id, onupdate="cascade", ondelete="cascade"))
    type = db.Column(db.String(10))
    record = db.Column(db.String(512))

    __table_args__ = (db.Index('records_index', "zone_id", "type"), )
