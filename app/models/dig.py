#!/usr/bin/env python
# coding: utf-8

from subprocess import run, PIPE


class DIG():
    def __init__(self, domain, dig_type='ANY', server=None, options=''):
        if server is not None:
            server = '@' + server
        else:
            server = ''

        self.result = run(['dig', dig_type, domain, server, options], stdout=PIPE)
        self.result = self.result.stdout
