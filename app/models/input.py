#!/usr/bin/env python
# coding: utf-8

import re

from app import log

class Input():
    @staticmethod
    def clear_domain(domain):
        log.debug('Start clearing domain: %s', domain)
        
        domain = domain.strip()
        domain = domain.split('@')[-1] # Remove prefixe if e-mail address
        domain = domain.replace('www.', '') # Remove www if present

        log.debug('Domain %s cleared', domain)

        return domain
