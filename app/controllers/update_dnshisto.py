#!/usr/bin/env python
# coding: utf8

import subprocess
import dns.resolver
from threading import Thread
from time import time

from sqlalchemy import and_

from app import db
from app.models.dnshisto import Domain, Zone, Record


#
# DOMAIN UPDATE
#
class DomainUpdate():
    # Initializing and getting the tld
    def __init__(self, tld):
        self.tld = tld
        self.nb = 50000 # Number of row to be added before commit
        print(("Initializing %s for domains" % tld))

    # Run domain update for tld
    def run(self):
        print(("Start domains patching for tld %s" % self.tld))

        # Opening zonefile in readonly
        with open('zonefiles/' + self.tld + '.ns', 'r') as f:
            print(("Zonefile opened for tld %s" % self.tld))
            print(("Reading the zonefile to patch domains for tld %s" % self.tld))

            bulk = []
            # Read the file line by line getting line number
            for i, line in enumerate(f):
                # Cleaning the line then append to bulk
                line = line.strip('\n').lower().encode('utf-8')
                bulk.append(line)

                # Add bulks of (self.nb) domains each time
                if i % self.nb == 0:
                    start_time = time()
                    ins = db.insert(Domain).prefix_with('IGNORE')
                    db.session.execute(ins, [dict(name=row) for row in bulk])

                    bulk = []
                    db.session.flush()
                    db.session.commit()
                    per_sec = self.nb / (time() - start_time)
                    print(("Add %s lines (%s lines) for tld %s (%s Domains/Sec)" % (self.nb, i, self.tld, per_sec)))

            if bulk:
                start_time = time()
                # Add last domains not covered by modulo
                ins = db.insert(Domain).prefix_with('IGNORE')
                db.session.execute(ins, [dict(name=row) for row in bulk])
                
                # Commit all domains
                db.session.flush()
                db.session.commit()
                per_sec = len(bulk) / (time() - start_time)
                bulk = []

                print(("Add rest of lines (%s lines) for tld %s (%s Domains/Sec)" % (i, self.tld, per_sec)))
                print(("All domains are commited for tld %s" % self.tld))
                print(("Domains patched for tld %s" % self.tld))
            else:
                print(('No new domain for tld %s' % self.tld))


#
# DIG UPDATE
#
class DigUpdate():

    # Initializing and getting the tld
    def __init__(self, tld):
        self.tld = tld
        print(("Initializing %s for dig update" % self.tld))

    # Run dig update for the tld
    def run(self):
        print(("Start dig updating for tld %s" % self.tld))
        [Thread(target=self.run_thread,args=(thread, )).start() for thread in range(0, 10)]

            
    def run_thread(self, thread_number):
        print('Start thread number %s' % thread_number)
        print(("Zonefile opened for tld %s in thread %s " % (self.tld, thread_number)))
        print(("Reading the zonefile to create new dig datas for tld %s in thread %s" % (self.tld, thread_number)))
        start_time = time()
        with open('zonefiles/' + self.tld + '.ns', 'r') as f:
            nb_dig = 100 # Number of digs before update
            bulk = []
            
            j = 0
            for i, line in enumerate(f):
                if int(str(i % 100)[0]) == thread_number:
                    j = j + 1
                    # Clean domain name
                    domain = line.strip('\n').lower().encode('utf-8')

                    # Proceed to dig
                    zones = self.dig(domain)
                    if zones:
                        # Query table domain to see know on which id the dig should be added
                        t_domain = db.session.query(Domain.id).filter(Domain.name == domain.decode('utf-8')).scalar()
                        if t_domain is not None:
                            # Add the new zone history
                            zone = Zone(domain_id=t_domain)
                            db.session.add(zone)
                            db.session.flush()

                            # Add all records registered for the zone
                            for record_type in zones:
                                for record in zones[record_type]:
                                    bulk.append(dict(zone_id=zone.id, type=record_type, record=record))

                            # Add bulks of (nb_dig) digs each time
                            if j % nb_dig == 0:
                                db.session.bulk_insert_mappings(
                                        Record,
                                        [record for record in bulk]
                                        )
                                db.session.commit()
                                bulk = []
                                per_sec = nb_dig / (time() - start_time)
                                start_time = time()
                                print(("Add %s lines (%s lines) for tld %s in thread %s (%s Digs/Sec)" % (nb_dig, j, self.tld, thread_number, per_sec)))
                                
                # Commit last digs not covered by modulo
            db.session.flush()
            db.session.commit()
            print(("Add rest of lines (%s lines) for tld %s in thread %s" % (i, self.tld, thread_number)))
            print(("Dig done for tld %s in thread %s" % (self.tld, thread_number)))

    def dig(self, domain):
        resolver = dns.resolver.Resolver()
        resolver.timeout = 0.2 
        resolver.lifetime = 0.2

        zones = {'ns': [], 'a': [], 'cname': [], 'mx': []}

        resolve = domain.decode('utf-8') + '.'
        # Resolve NameServer, pass if exception is raised in order to continue, no log to be done
        try:
            for data in resolver.query(resolve, 'NS'):
                zones['ns'].append(data.to_text())
        except:
            pass

        # Resolve a/cname/mx only if ns has been resolved (for performance issue)
        if zones['ns']:
            try:
                for data in resolver.query(resolve, 'A'):
                    zones['a'].append(data.to_text())
            except:
                pass
            try:
                for data in resolver.query(resolve, 'CNAME'):
                    zones['cname'].append(data.to_text())
            except:
                pass
            try:
                for data in resolver.query(resolve, 'MX'):
                    zones['mx'].append(data.to_text())
            except:
                pass

            return zones
        else:
            return False
