#!/usr/bin/env python
# coding: utf-8
from flask import Blueprint, abort, jsonify

from app.models.whois import WHOIS

mod_whois = Blueprint('whois', __name__, url_prefix='/whois')

@mod_whois.route('/<string:domain>', methods=['GET'])
def whois(domain):
    whois = WHOIS(domain)
    whois.run()
    return jsonify(domain=whois.domain, whois=whois.whois, expire=whois.expire), 200
