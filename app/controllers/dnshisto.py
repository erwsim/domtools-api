#!/usr/bin/env python
# coding: utf-8

from flask import Blueprint, abort, jsonify

from app import db
from app.models.dnshisto import Domain, Zone, Record
from app.models.input import Input


mod_dnshisto = Blueprint('dnshisto', __name__, url_prefix='/dnshisto')

@mod_dnshisto.route('/<string:domain>', methods=['GET'])
def dnshisto(domain):
    domain = Input.clear_domain(domain)
    histo = db.session.query(Record.type, Record.record, Zone.date).join(Zone).join(Domain).filter(Domain.name == domain).all()
    if histo:
        history = {}
        for record in histo:
            date = record.date.strftime('%d-%m-%Y')
            if date not in history:
                history[date] = {} 

            if not record.type in history[date]:
                history[date][record.type] = ''
            history[date][record.type] = history[date][record.type] + record.record + ';'
        return jsonify(domain=domain, history=history), 200
    else:
        return jsonify(none='none'), 204
