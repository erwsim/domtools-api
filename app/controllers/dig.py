#!/usr/bin/env python
# coding: utf-8
from flask import Blueprint, abort, jsonify

from app.models.dig import DIG
from app.models.input import Input

mod_dig = Blueprint('dig', __name__, url_prefix='/dig')


@mod_dig.route('/<string:domain>', methods=['GET'])
@mod_dig.route('/<string:domain>/<string:dig_type>', methods=['GET'])
@mod_dig.route('/<string:domain>/<string:dig_type>/<string:server>', methods=['GET'])
@mod_dig.route('/<string:domain>/<string:dig_type>/<string:server>/<string:options>', methods=['GET'])
def dig(domain, dig_type='ANY', server=None, options=''):
    domain = Input.clear_domain(domain)
    dig = DIG(domain, dig_type, server, options)

    return jsonify(domain=domain, dig=dig.result.decode('utf-8')), 200
