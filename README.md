# DOMTools README

#
# 1. DEFINE .NETRC
#

To download Zone Files by FTP, accesses have to be defined in .netrc file in the home directory.
Example of a .netrc file

machine <ftp>
    login <login>
    password <password>

#
# 2. CREATE NS FILE
#

For each tld access a ns file must be created is zonefiles folder.
For example org.ns for .org
The file must contain the tld name


#
# 3. CHANGE CONFIG.SAMPLE.PY
#

The file config.sample.py must be changed accordly to your configuration (especialy database credentials).
Once file will be modified the file must be renamed to config.py
The tables will be automatically created.
