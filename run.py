#!/usr/bin/env python
# coding: utf8

from app import app
import config

if __name__ == '__main__':
    app.run(debug=config.app['debug'], threaded=config.app['threaded'])
