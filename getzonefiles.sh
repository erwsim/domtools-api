# COM
sh zonebin/com.sh

# NET
sh zonebin/net.sh

# NAME
sh zonebin/name.sh

# FR
sh zonebin/fr.sh

# INFO
sh zonebin/info.sh

# ORG
sh zonebin/org.sh
