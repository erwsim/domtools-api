#
# com
#

# Download zone file
lftp -e "get com.zone.gz -o zonefiles/com.zone.gz; bye" rz.verisign-grs.com 

# Extract zone file
cd zonefiles
gunzip com.zone.gz

# Cleaning the file to have just domains and unique, final file
sed -ie "/ NS/\!d" com.zone
sed -ie "s/ NS.*//gI" com.zone
sed -ie "s/.*/\L&/g" com.zone
sed -ie "s/$/.com/g" com.zone
cat com.zone | sort | uniq > com.ns

# Deleting all unwanted files
rm com.zone
rm com.zonee