#
# name
#

# Download zone file
lftp -e "get master.name.zone.gz -o zonefiles/name.zone.gz; bye" rzname.verisign-grs.com

# Extract zone file
cd zonefiles
gunzip name.zone.gz

# Cleaning the file to have just domains and unique, final file
sed -ie "/ NS/\!d" name.zone
sed -ie "s/ NS.*//gI" name.zone
sed -ie "s/.*/\L&/g" name.zone
sed -ie "s/$/.name/g" name.zone
cat name.zone | sort | uniq > name.ns

# Deleting all unwanted files
rm name.zone
rm name.zonee