#
# net
#

# Download zone file
lftp -e "get net.zone.gz -o zonefiles/net.zone.gz; bye" rz.verisign-grs.com

# Extract zone file
cd zonefiles
gunzip net.zone.gz

# Cleaning the file to have just domains and unique, final file
sed -ie "/ NS/\!d" net.zone
sed -ie "s/ NS.*//gI" net.zone
sed -ie "s/.*/\L&/g" net.zone
sed -ie "s/$/.net/g" net.zone
cat net.zone | sort | uniq > net.ns

# Deleting all unwanted files
rm net.zone
rm net.zonee
