#
# info
#

# Download zone file
lftp -e "get zonefile/info.zone.gz -o zonefiles/info.zone.gz; bye" ftp.afilias.net

# Extract zone file
cd zonefiles
gunzip info.zone.gz

# Cleaning the file to have just domains and unique, final file
sed -ie "/. NS/\!d" info.zone
sed -ie "s/. NS.*//gI" info.zone
sed -ie "s/.*/\L&/g" info.zone
cat info.zone | sort | uniq > info.ns

# Deleting all unwanted files
rm info.zone
rm info.zonee