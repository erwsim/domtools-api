#
# org
#

# Download zone file
lftp -e "get zonefile/org.zone.gz -o zonefiles/org.zone.gz; bye" dn1.publicinterestregistry.net

# Extract zone file
cd zonefiles
gunzip org.zone.gz

# Cleaning the file to have just domains and unique, final file
sed -ie "s/. NS/\!d/g" org.zone
sed -ie "s/. NS.*//gI" org.zone
sed -ie "s/.*/\L&/g" org.zone
cat org.zone | sort | uniq > org.ns

# Deleting all unwanted files
rm org.zone
rm org.zonee