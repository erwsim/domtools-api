#
# fr
#

cd zonefiles

DATE=$(($(date +%Y%m) - 2))
wget https://www.afnic.fr/data/opendata/"$DATE"_OPENDATA_A-NomsDeDomaineEnPointFr.zip
unzip "$DATE"_OPENDATA_A-NomsDeDomaineEnPointFr.zip; mv "$DATE"_OPENDATA_A-NomsDeDomaineEnPointFr.csv fr.zone

# For compatibilities
cp fr.zone fr.ns

# Cleaning the file to have just domains, final file
cat fr.ns | perl -CS -pe 's/;.*//g' > fr.clean
sed -ie "1,4d" fr.clean
cat fr.clean | uniq > fr.ns

# Deleting all unwanted files
rm fr.zone
rm fr.cleane
rm fr.clean
rm "$DATE"_OPENDATA_A-NomsDeDomaineEnPointFr.zip 
