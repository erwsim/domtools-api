#!/usr/bin/env python
# coding: utf8
import sys, getopt

from app.controllers.update_dnshisto import DomainUpdate, DigUpdate

#
# Launch Update script
# For best performance update cannot be done on all registry at once
#
def launch_update(argv):
    try:
        opts, args = getopt.getopt(argv,"hd:z:")
    except getopt.GetoptError:
        print('launch_update.py [-h] -d <tld> -z <tld>')
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print('launch_update.py [-h] -d <tld> -z <tld>')
            sys.exit(2)
        if opt == '-d':
            print (arg)
            domain = DomainUpdate(arg)
            domain.run()
        if opt == '-z':
            dig = DigUpdate(arg)
            dig.run()

launch_update(sys.argv[1:])
