#!/usr/bin/env python
# coding: utf8

# GENERAL CONFIG
mysql = {'host': 'localhost', # Fill DB Host
        'port': 3306, # Fill DB Port
        'user': 'username', # Fill DB username
        'password': 'password', # Fill user password
        'db': 'db'} # Fill DB

app =   {'debug': True,
        'threaded': True,
	'secret_key': '1234', # Fill a secret key
        'workers': 8}

# SQL ALCHEMY CONFIG (require GENERAL CONFIG TO BE SET)
SQLALCHEMY_DATABASE_URI = "mysql://%s:%s@%s:%s/%s" % (mysql['user'], mysql['password'], mysql['host'], mysql['port'], mysql['db'])
SQLALCHEMY_TRACK_MODIFICATIONS = True
SQLALCHEMY_POOL_RECYCLE = 3600
